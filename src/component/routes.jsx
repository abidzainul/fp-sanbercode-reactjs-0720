import React from 'react';
import { Route, Switch } from "react-router-dom";

import HomeMovies from './movie/Home';
import MovieDetail from './movie/MovieDetail';
import HomeGames from './game/Home';
import GameDetail from './game/GameDetail';
import Movies from './admin/movie/index';
import Games from './admin/game/index';
import Users from './admin/user/index';
import Login from './auth/Login';
import Register from './auth/Register';

const routes = (
    <Switch>
        <Route path="/" exact component={HomeMovies} />
        <Route path="/movies" component={HomeMovies} />
        <Route path="/preview-movie" component={MovieDetail} />
        <Route path="/games" component={HomeGames} />
        <Route path="/preview-game" component={GameDetail} />
        <Route path="/admin/movies" component={Movies} />
        <Route path="/admin/games" component={Games} />
        <Route path="/admin/users" component={Users} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
    </Switch>
)

export default routes;
