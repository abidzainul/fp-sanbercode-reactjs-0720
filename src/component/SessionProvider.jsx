import React, { useState, createContext } from 'react';

export const SessionContext = createContext();

export const SessionProvider = props => {
    const [session, setSession] = useState ({
        username: null,
        password: null,
    })
  
    return (
      <SessionContext.Provider 
        value={[session, setSession]}>
        {props.children}
      </SessionContext.Provider>
    );
  };