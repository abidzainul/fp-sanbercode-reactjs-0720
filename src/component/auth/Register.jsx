import React, { useContext, useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { SessionContext } from '../SessionProvider';
import { insertUser } from '../../services/ApiServices'
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Register = () => {
    const classes = useStyles();
    let history = useHistory();
    const [session, setSession] = useContext(SessionContext);
    const [login, setLogin] = useState({
        username: "",
        password: "",
    })
    const [errors, setErrors] = useState("Username atau Password harus diisi!!")

    useEffect(() => {
        document.title = "Register"
    }, []);

    const handleChange = (event) => {
        const value = event.target.value;
        setLogin({
            ...login,
            [event.target.name]: value
        });
    }

    const handleValidation = () => {
        let valid = true;
        let error = ""

        if (login.username === "") {
            valid = false;
            error = error.concat("Username tidak boleh kosong\n");
        }

        if (login.password === "") {
            valid = false;
            error = error.concat("Password tidak boleh kosong\n");
        }

        setErrors(error);
        return valid;
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if (handleValidation()) {
            insertUser(login)
                .then(res => {
                    console.log(res.data.id)
                    if (typeof res.data.id === 'undefined') {
                        alert(res.data)
                    } else {
                        setSession(login)
                        history.push("/login");
                    }
                }).catch(e => {
                    console.log(`error: ${e}`)
                })
        } else {
            console.log(errors)
            alert(errors)
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <Box mt={20}>
            </Box>
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <PersonAddIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign Up
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                        onChange={handleChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={handleChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign Up
                    </Button>
                    <Grid container>
                        <Grid item>
                            <Link href="/login" variant="body2">
                                {"Ready have an account? Sign In"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={25}>
            </Box>
        </Container>
    );
}

export default Register