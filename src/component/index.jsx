import React from 'react';
import {
    Container,
    Box,
} from '@material-ui/core';
import { BrowserRouter as Router } from "react-router-dom";
import routes from './routes';
import Navbar from './base/Navbar'
import { SessionProvider } from './SessionProvider';

const Index = () => {
    return (
        <SessionProvider>
            <Router>
                <div>
                    <Navbar />

                    <Container fixed>
                        {routes}
                    </Container>
                    <Box display="flex" justifyContent="center" bgcolor="text.primary" color="white" mt={4}>
                        <h4>copyright &copy; 2020 by Sanbercode</h4>
                    </Box>
                </div>
            </Router>
        </SessionProvider>
    )
}

export default Index