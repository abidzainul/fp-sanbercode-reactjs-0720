import React, { useEffect, useState, useContext } from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardActionArea,
    CardMedia,
    Typography,
    CardContent,
    ButtonBase,
    CardActions,
    Paper,
    Grid,
    Box
} from '@material-ui/core';

import IconStar from '@material-ui/icons/Star';
import { getMovie } from '../../services/ApiServices'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));

const MovieDetail = (props) => {
    const classes = useStyles();
    const [movie, setMovie] = useState({})

    useEffect(() => {
        document.title = "Home"
        console.log('data: ' + props.location.data)
        loadMovie();
    }, []);

    const loadMovie = () => {
        getMovie(props.location.data.id)
            .then(response => {
                console.log(response.data);
                setMovie(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div className={classes.root}>
            <Box mt={5}>
            </Box>
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item>
                        <img className={classes.img} alt="complex" src={movie.image_url} />
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    {movie.title}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    {movie.description}
                                </Typography>
                                <Typography variant="body2" color="textSecondary">
                                {movie.year}, {movie.genre}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body2">
                                    Durasi: {movie.duration} Menit
                            </Typography>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant="subtitle1">Rating: {movie.rating}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );

}

export default MovieDetail
