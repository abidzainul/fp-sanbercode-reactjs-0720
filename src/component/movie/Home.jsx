import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardActionArea,
    CardMedia,
    Typography,
    CardContent,
    Button,
    CardActions,
    GridList,
    Grid,
} from '@material-ui/core';
import { Link } from "react-router-dom";

import { getAllMovies } from '../../services/ApiServices'

const useStyles = makeStyles({
    card: {
        margin: '10px',
    },
    subtitle: {
        display: '-webkit-box',
        WebkitBoxOrient: 'vertical',
        WebkitLineClamp: 1,
        overflow: 'hidden',
        height: '70px',
    },
    subtitle2: {
        display: '-webkit-box',
        WebkitBoxOrient: 'vertical',
        WebkitLineClamp: 1,
        overflow: 'hidden',
        height: '20px',
    },
    link: {
        textDecoration: 'none'
    }
});

const MovieItem = (props) => {
    const classes = useStyles()

    return (
        (
            <Grid item xs={3}>
                <Card className={classes.card}>
                    <Link color="inherit"
                        to={{ pathname: "/preview-movie", data: props.data }}
                        className={classes.link}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="150"
                                image={props.data.image_url}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" noWrap>
                                    {props.data.title}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p" className={classes.subtitle}>
                                    {props.data.description != null ? props.data.description.substr(0, 100) : null}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p" className={classes.subtitle2}>
                                    {props.data.year}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Link>

                    <CardActions>
                        <Link color="inherit"
                            to={{ pathname: "/preview-movie", data: props.data }}
                            className={classes.link}>
                            <Button size="small" color="primary">
                                Preview
                        </Button>
                        </Link>
                    </CardActions>
                </Card>
            </Grid>
        )
    )

}

class Home extends Component {
    constructor() {
        super();
        this.state = {
            movies: []
        }
    }

    componentDidMount() {
        document.title = "Home"
        this.fetchMovies();
    }

    fetchMovies() {
        getAllMovies()
            .then(response => {
                console.log(response.data);
                this.setState({
                    movies: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        if (this.state.movies.length === 0) {
            return <h1>Tidak ada Film</h1>
        }

        return (
            <div>
                <h1>Daftar Film</h1>
                <GridList>
                    {this.state.movies.map((movie, i) => (
                        <MovieItem
                            key={i}
                            data={movie} />
                    ))}
                </GridList>
            </div>
        )
    }
}

export default Home
