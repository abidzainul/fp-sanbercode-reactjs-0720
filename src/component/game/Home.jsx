import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardActionArea,
    CardMedia,
    Typography,
    CardContent,
    Button,
    CardActions,
    GridList,
    Grid,
} from '@material-ui/core';

import { getAllGames } from '../../services/ApiServices'


const useStyles = makeStyles({
    card: {
        margin: '10px',
    },
    subtitle: {
        display: '-webkit-box',
        WebkitBoxOrient: 'vertical',
        WebkitLineClamp: 1,
        overflow: 'hidden',
        height: '70px',
    },
    subtitle2: {
        display: '-webkit-box',
        WebkitBoxOrient: 'vertical',
        WebkitLineClamp: 1,
        overflow: 'hidden',
        height: '20px',
    },
    link: {
        textDecoration: 'none'
    }
});

const GameItem = (props) => {
    const classes = useStyles()

    return (
        (
            <Grid item xs={3}>
                <Card className={classes.card}>
                    <Link color="inherit"
                        to={{ pathname: "/preview-game", data: props.data }}
                        className={classes.link}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="150"
                                image={props.data.image_url}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" noWrap>
                                    {props.data.name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p" className={classes.subtitle}>
                                    {props.data.genre}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p" className={classes.subtitle2}>
                                    {props.data.release}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Link>
                    <CardActions>
                        <Link color="inherit"
                            to={{ pathname: "/preview-game", data: props.data }}
                            className={classes.link}>
                            <Button size="small" color="primary">
                                Preview
                        </Button>
                        </Link>
                    </CardActions>
                </Card>
            </Grid>
        )
    )

}

class Home extends Component {
    constructor() {
        super();
        this.state = {
            games: []
        }
    }

    componentDidMount() {
        document.title = "Home"
        this.fetchGames();
    }

    fetchGames() {
        getAllGames()
            .then(response => {
                console.log(response.data);
                this.setState({
                    games: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        if (this.state.games.length === 0) {
            return <h1>Tidak ada Game</h1>
        }

        return (
            <div>
                <h1>Daftar Game</h1>
                <GridList>
                    {this.state.games.map((movie, i) => (
                        <GameItem
                            key={i}
                            data={movie} />
                    ))}
                </GridList>
            </div>
        )
    }
}

export default Home
