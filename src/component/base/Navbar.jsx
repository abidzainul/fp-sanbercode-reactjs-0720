import React, { Component, useContext } from "react";
import {
    AppBar,
    Toolbar,
    Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import { SessionContext } from '../SessionProvider';


const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1,
    },
    nav: {
        textDecoration: 'none',
        color: 'white',
        padding: '10px',
        fontSize: '16px',
        fontWeight: 'bold',
    }
}));

const NavAdmin = (props) => {
    const classes = useStyles();
    if(props.session.username === null){
        return <div></div>
    }
    return (
        <div>
        <Link to="/admin/movies" className={classes.nav}>
            MANAGE MOVIE
        </Link>
        <Link to="/admin/games" className={classes.nav}>
            MANAGE GAME
        </Link>
        <Link to="/admin/users" className={classes.nav}>
            MANAGE USER
        </Link>
        </div>
    )
}

const Navbar = () => {
    const classes = useStyles();
    const [session, setSession] = useContext(SessionContext);

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    SANBERCODE
                </Typography>
                <Link to="/movies" className={classes.nav}>
                    MOVIE
                </Link>
                <Link to="/games" className={classes.nav}>
                    GAME
                </Link>
                <NavAdmin session={session}/>
                <Link to="/login" className={classes.nav}>
                    LOGIN
                </Link>
            </Toolbar>
        </AppBar>
    )
}

export default Navbar;