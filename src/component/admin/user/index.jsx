import React, { Component, useEffect, useContext } from 'react';

import { UserProvider } from './UserProvider';
import UserList from './UserList'
import FormDialogUser from './FormDialogUser'

class Users extends Component {
    
    componentDidMount(){
        document.title = "Managemen Users"
    }

    render(){
        return (
            <UserProvider>
                <UserList />
                <FormDialogUser />
            </UserProvider>
        )
    }
}

export default Users