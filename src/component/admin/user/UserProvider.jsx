import React, { useState, createContext } from 'react';

export const UserContext = createContext();

export const UserProvider = props => {
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState({
        id: "",
        username: "",
        password: "",
    })
    const [isUpdate, setIsUpdate] = useState(false)

    return (
        <UserContext.Provider
            value={{
                list: [users, setUsers],
                data: [user, setUser],
                update: [isUpdate, setIsUpdate]
            }}>

            {props.children}
        </UserContext.Provider>
    );
};