import React, { useEffect, useState, useContext } from 'react';
import { getAllUsers, removeUser } from '../../../services/ApiServices';
import { UserContext } from './UserProvider';
import {
    Button,
    Box,
} from '@material-ui/core';
import TableUser from './TableUser';
import FormDialog from './FormDialogUser';
import IconDeleteOutline from '@material-ui/icons/DeleteOutline';
import IconEdit from '@material-ui/icons/Edit';
import { SessionContext } from '../../SessionProvider';

const UserList = () => {
    const { list, data, update } = useContext(UserContext);
    const [listUser, setListUser] = list
    const [user, setUser] = data
    const [isUpdate, setIsUpdate] = update
    const [isOpen, setIsOpen] = useState(false)
    const [session, setSession] = useContext(SessionContext);


    useEffect(() => {
        fetchUsers();
    }, []);

    const fetchUsers = () => {
        getAllUsers()
            .then(res => {
                console.log(res.data);
                setListUser(res.data);
            }).catch(e => {
                console.log(e)
            })
    }

    const onEdit = (id, event) => {
        console.log(id)
        let data = listUser.find(user => user.id === id)

        console.log(data);
        setUser(data)

        setIsUpdate(true)
        handleDialogOpen()
    }

    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        removeUser(id)
            .then(res => {
                console.log("delete")
                fetchUsers()
            }).catch(e => {
                console.log(e)
            })

        setIsUpdate(false)
    }

    const reset = () => {
        setUser({
            id: "",
            username: "",
            password: "",
        })

        setIsUpdate(false)
    }

    const onCreate = () => {
        reset()
        handleDialogOpen();
    }

    const handleDialogOpen = () => {
        setIsOpen(true);
    }

    const handleDialogClose = () => {
        setIsOpen(false);
    };
    
    if (session.username === null) {
        return (
            <div>
                <h1>Anda Belum Login</h1>
                <h1>Login untuk melanjutkan ke fitur ini</h1>
            </div>
        )
    }

    return (
        <div>
            <Box display="flex" flexDirection="row-reverse" mb={2} mt={2}>
                <Button variant="contained" color="primary" onClick={onCreate}>
                    Add User
                </Button>
            </Box>

            <div style={{ maxWidth: "100%" }}>
                <TableUser
                    data={listUser.sort((a, b) => a.username < b.username ? -1 : 1)}
                    actions={[
                        {
                            icon: () => <IconEdit color="primary" />,
                            tooltip: "Update",
                            onClick: (event, rowData) => onEdit(rowData.id, event)
                        },
                        {
                            icon: () => <IconDeleteOutline color="secondary" />,
                            tooltip: "Delete",
                            onClick: (event, rowData) => onDelete(rowData.id, event)
                        }
                    ]}
                />
            </div>

            <FormDialog
                isOpen={isOpen}
                handleClose={handleDialogClose} />
        </div>
    )

}

export default UserList