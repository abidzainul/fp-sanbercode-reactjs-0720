import React, { useState, useEffect, useContext } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
  TextField,
} from '@material-ui/core';
import {
  getAllUsers,
  insertUser,
  updateUser,
} from '../../../services/ApiServices';
import { UserContext } from './UserProvider';

const FormDialogUser = ({ isOpen = false, handleClose }) => {
  const { list, data, update } = useContext(UserContext);
  const [listUser, setListUser] = list
  const [user, setUser] = data
  const [isUpdate, setIsUpdate] = update

  const [errors, setErrors] = useState("Field tidak boleh kosong!!")

  const fetchUsers = () => {
    getAllUsers()
      .then(res => {
        console.log(res.data);
        setListUser(res.data);
      }).catch(e => {
        console.log(e)
      })
  }

  const handleChange = (event) => {
    const value = event.target.value;
    setUser({
      ...user,
      [event.target.name]: value
    });
  }
  

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(user)

    
    if (isUpdate) {
        updateUser(user.id, user)
          .then(res => {
            console.log(`update: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      } else {
        insertUser(user)
          .then(res => {
            console.log(`insert: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      }

  }

  const reset = () => {
    setUser({
      id: "",
      username: "",
      password: "",
    })

    setIsUpdate(false)
    fetchUsers()
    handleClose()
  }

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
        <form onSubmit={handleSubmit}>
          <DialogTitle id="form-dialog-title">Tambah User</DialogTitle>
          <DialogContent>
            <DialogContentText>
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              name="username"
              label="Username"
              type="text"
              multiline
              fullWidth
              value={user.username ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="password"
              label="Password"
              type="text"
              multiline
              fullWidth
              value={user.password ?? ""}
              onChange={handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="secondary">
              Cancel
          </Button>
            <Button type="submit" color="primary">
              Simpan
          </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

export default FormDialogUser
