import React, { useState, useEffect, useContext } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
  TextField,
} from '@material-ui/core';
import {
  getAllGames,
  insertGame,
  updateGame,
} from '../../../services/ApiServices';
import { GameContext } from './GameProvider';

const FormDialogGame = ({ isOpen = false, handleClose }) => {
  const { list, data, update } = useContext(GameContext);
  const [listGame, setListGame] = list
  const [game, setGame] = data
  const [isUpdate, setIsUpdate] = update

  const [errors, setErrors] = useState("Field tidak boleh kosong!!")

  const fetchGames = () => {
    getAllGames()
      .then(res => {
        console.log(res.data);
        setListGame(res.data);
      }).catch(e => {
        console.log(e)
      })
  }

  const handleChange = (event) => {
    const value = event.target.value;
    setGame({
      ...game,
      [event.target.name]: value
    });
  }

  const handleValidation = () => {
    let valid = true;
    let error = ""

    if (game.name === "") {
      valid = false;
      error = error.concat("Judul tidak boleh kosong\n");
    }

    if (game.genre === "") {
      valid = false;
      error = error.concat("Deskripsi tidak boleh kosong\n");
    }

    if (game.singlePlayer === "") {
      valid = false;
      error = error.concat("Tahun tidak boleh kosong\n");
    }

    if (game.multiplayer === "") {
      valid = false;
      error = error.concat("Durasi tidak boleh kosong\n");
    }

    if (game.platform === "") {
      valid = false;
      error = error.concat("Genre tidak boleh kosong\n");
    }

    if (game.release === "") {
      valid = false;
      error = error.concat("Rating tidak boleh kosong\n");
    }

    setErrors(error);
    return valid;
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(game)

    if (handleValidation()) {
      if (isUpdate) {
        updateGame(game.id, game)
          .then(res => {
            console.log(`update: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      } else {
        insertGame(game)
          .then(res => {
            console.log(`insert: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      }
    } else {
      console.log(errors)
      alert(errors)
    }
  }

  const reset = () => {
    setGame({
        id: "",
        name: "",
        genre: "",
        singlePlayer: "",
        multiplayer: "",
        platform: "",
        release: "",
        image_url: "",
    })

    setIsUpdate(false)
    fetchGames()
    handleClose()
  }

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
        <form onSubmit={handleSubmit}>
          <DialogTitle id="form-dialog-title">Tambah Game</DialogTitle>
          <DialogContent>
            <DialogContentText>
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              name="name"
              label="Nama"
              type="text"
              multiline
              fullWidth
              value={game.name ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="genre"
              label="Genre"
              type="text"
              multiline
              fullWidth
              value={game.genre ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="singlePlayer"
              label="Single Player"
              type="number"
              fullWidth
              value={game.singlePlayer ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="multiplayer"
              label="Multi Player"
              type="number"
              fullWidth
              value={game.multiplayer ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="platform"
              label="Platform"
              type="text"
              fullWidth
              value={game.platform ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="release"
              label="Release"
              type="number"
              fullWidth
              value={game.release ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="image_url"
              label="Image"
              type="text"
              multiline
              fullWidth
              value={game.image_url ?? ""}
              onChange={handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="secondary">
              Cancel
          </Button>
            <Button type="submit" color="primary">
              Simpan
          </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

export default FormDialogGame
