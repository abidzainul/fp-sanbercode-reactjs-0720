import React, { useState, createContext } from 'react';

export const GameContext = createContext();

export const GameProvider = props => {
    const [games, setGames] = useState([]);
    const [game, setGame] = useState({
        id: "",
        name: "",
        genre: "",
        singlePlayer: "",
        multiplayer: "",
        platform: "",
        release: "",
        image_url: "",
    })
    const [isUpdate, setIsUpdate] = useState(false)

    return (
        <GameContext.Provider
            value={{
                list: [games, setGames],
                data: [game, setGame],
                update: [isUpdate, setIsUpdate]
            }}>

            {props.children}
        </GameContext.Provider>
    );
};