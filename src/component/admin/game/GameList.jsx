import React, { useEffect, useState, useContext } from 'react';
import { getAllGames, removeGame } from '../../../services/ApiServices';
import { GameContext } from './GameProvider';
import {
    Button,
    Box,
} from '@material-ui/core';
import TableGame from './TableGame';
import FormDialog from './FormDialogGame';
import IconDeleteOutline from '@material-ui/icons/DeleteOutline';
import IconEdit from '@material-ui/icons/Edit';
import { SessionContext } from '../../SessionProvider';

const GameList = () => {
    const { list, data, update } = useContext(GameContext);
    const [listGame, setListGame] = list
    const [game, setGame] = data
    const [isUpdate, setIsUpdate] = update
    const [isOpen, setIsOpen] = useState(false)
    const [session, setSession] = useContext(SessionContext);


    useEffect(() => {
        fetchGames();
    }, []);

    const fetchGames = () => {
        getAllGames()
            .then(res => {
                console.log(res.data);
                setListGame(res.data);
            }).catch(e => {
                console.log(e)
            })
    }

    const onEdit = (id, event) => {
        console.log(id)
        let data = listGame.find(game => game.id === id)

        console.log(data);
        setGame(data)

        setIsUpdate(true)
        handleDialogOpen()
    }

    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        removeGame(id)
            .then(res => {
                console.log("delete")
                fetchGames()
            }).catch(e => {
                console.log(e)
            })

        setIsUpdate(false)
    }

    const reset = () => {
        setGame({
            id: "",
            name: "",
            genre: "",
            singlePlayer: "",
            multiplayer: "",
            platform: "",
            release: "",
            image_url: "",
        })

        setIsUpdate(false)
    }

    const onCreate = () => {
        reset()
        handleDialogOpen();
    }

    const handleDialogOpen = () => {
        setIsOpen(true);
    }

    const handleDialogClose = () => {
        setIsOpen(false);
    };
    
    if (session.username === null) {
        return (
            <div>
                <h1>Anda Belum Login</h1>
                <h1>Login untuk melanjutkan ke fitur ini</h1>
            </div>
        )
    }

    return (
        <div>
            <Box display="flex" flexDirection="row-reverse" mb={2} mt={2}>
                <Button variant="contained" color="primary" onClick={onCreate}>
                    Add Game
                </Button>
            </Box>

            <div style={{ maxWidth: "100%" }}>
                <TableGame
                    data={listGame.sort((a, b) => a.release < b.release ? 1 : -1)}
                    actions={[
                        {
                            icon: () => <IconEdit color="primary" />,
                            tooltip: "Update",
                            onClick: (event, rowData) => onEdit(rowData.id, event)
                        },
                        {
                            icon: () => <IconDeleteOutline color="secondary" />,
                            tooltip: "Delete",
                            onClick: (event, rowData) => onDelete(rowData.id, event)
                        }
                    ]}
                />
            </div>

            <FormDialog
                isOpen={isOpen}
                handleClose={handleDialogClose} />
        </div>
    )

}

export default GameList