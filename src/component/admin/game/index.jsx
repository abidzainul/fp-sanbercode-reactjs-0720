import React, { Component, useEffect, useContext } from 'react';

import { GameProvider } from './GameProvider';
import GameList from './GameList'
import FormDialogGame from './FormDialogGame'

class Games extends Component {
    
    componentDidMount(){
        document.title = "Managemen Games"
    }

    render(){
        return (
            <GameProvider>
                <GameList />
                <FormDialogGame />
            </GameProvider>
        )
    }
}

export default Games