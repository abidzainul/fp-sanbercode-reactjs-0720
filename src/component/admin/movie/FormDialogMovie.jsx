import React, { useState, useEffect, useContext } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Button,
  TextField,
} from '@material-ui/core';
import {
  getAllMovies,
  insertMovie,
  updateMovie,
} from '../../../services/ApiServices';
import { MovieContext } from './MovieProvider';

const FormDialogMovie = ({ isOpen = false, handleClose }) => {
  const { list, data, update } = useContext(MovieContext);
  const [listMovie, setListMovie] = list
  const [movie, setMovie] = data
  const [isUpdate, setIsUpdate] = update

  const [errors, setErrors] = useState("Field tidak boleh kosong!!")

  const fetchMovies = () => {
    getAllMovies()
      .then(res => {
        console.log(res.data);
        setListMovie(res.data);
      }).catch(e => {
        console.log(e)
      })
  }

  const handleChange = (event) => {
    const value = event.target.value;
    setMovie({
      ...movie,
      [event.target.name]: value
    });
  }

  const handleValidation = () => {
    let valid = true;
    let error = ""

    if (movie.title === "") {
      valid = false;
      error = error.concat("Judul tidak boleh kosong\n");
    }

    if (movie.description === "") {
      valid = false;
      error = error.concat("Deskripsi tidak boleh kosong\n");
    }

    if (movie.year === "") {
      valid = false;
      error = error.concat("Tahun tidak boleh kosong\n");
    }

    if (movie.duration === "") {
      valid = false;
      error = error.concat("Durasi tidak boleh kosong\n");
    }

    if (movie.genre === "") {
      valid = false;
      error = error.concat("Genre tidak boleh kosong\n");
    }

    if (movie.rating === "") {
      valid = false;
      error = error.concat("Rating tidak boleh kosong\n");
    }

    setErrors(error);
    return valid;
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(movie)

    if (handleValidation()) {
      if (isUpdate) {
        updateMovie(movie.id, movie)
          .then(res => {
            console.log(`update: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      } else {
        insertMovie(movie)
          .then(res => {
            console.log(`insert: ${res}`)
            reset()
          }).catch(e => {
            console.log(`error: ${e}`)
          })
      }
    } else {
      console.log(errors)
      alert(errors)
    }
  }

  const reset = () => {
    setMovie({
      id: "",
      title: "",
      description: "",
      year: "",
      duration: "",
      genre: "",
      rating: "",
      review: "",
      image_url: "",
    })

    setIsUpdate(false)
    fetchMovies()
    handleClose()
  }

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
        <form onSubmit={handleSubmit}>
          <DialogTitle id="form-dialog-title">Tambah Movie</DialogTitle>
          <DialogContent>
            <DialogContentText>
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              name="title"
              label="Judul"
              type="text"
              multiline
              fullWidth
              value={movie.title ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="description"
              label="Deskripsi"
              type="text"
              multiline
              fullWidth
              value={movie.description ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="year"
              label="Tahun"
              type="number"
              fullWidth
              value={movie.year ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="duration"
              label="Durasi"
              type="number"
              fullWidth
              value={movie.duration ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="genre"
              label="Genre"
              type="text"
              fullWidth
              value={movie.genre ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="rating"
              label="Rating"
              type="number"
              fullWidth
              value={movie.rating ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="review"
              label="Review"
              type="text"
              multiline
              fullWidth
              value={movie.review ?? ""}
              onChange={handleChange}
            />
            <TextField
              autoFocus
              margin="dense"
              name="image_url"
              label="Image"
              type="text"
              multiline
              fullWidth
              value={movie.image_url ?? ""}
              onChange={handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="secondary">
              Cancel
          </Button>
            <Button type="submit" color="primary">
              Simpan
          </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

export default FormDialogMovie
