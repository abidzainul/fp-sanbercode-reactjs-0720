import React, { useEffect, useState, useContext } from 'react';
import { getAllMovies, removeMovie } from '../../../services/ApiServices';
import { MovieContext } from './MovieProvider';
import {
    Button,
    Box,
} from '@material-ui/core';
import TableMovie from './TableMovie';
import FormDialog from './FormDialogMovie';
import IconDeleteOutline from '@material-ui/icons/DeleteOutline';
import IconEdit from '@material-ui/icons/Edit';
import { SessionContext } from '../../SessionProvider';

const MovieList = () => {
    const { list, data, update } = useContext(MovieContext);
    const [listMovie, setListMovie] = list
    const [movie, setMovie] = data
    const [isUpdate, setIsUpdate] = update
    const [isOpen, setIsOpen] = useState(false)
    const [session, setSession] = useContext(SessionContext);


    useEffect(() => {
        fetchMovies();
    }, []);

    const fetchMovies = () => {
        getAllMovies()
            .then(res => {
                console.log(res.data);
                setListMovie(res.data);
            }).catch(e => {
                console.log(e)
            })
    }

    const onEdit = (id, event) => {
        console.log(id)
        let data = listMovie.find(movie => movie.id === id)

        console.log(data);
        setMovie(data)

        setIsUpdate(true)
        handleDialogOpen()
    }

    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        removeMovie(id)
            .then(res => {
                console.log("delete")
                fetchMovies()
            }).catch(e => {
                console.log(e)
            })

        setIsUpdate(false)
    }

    const reset = () => {
        setMovie({
            id: "",
            title: "",
            description: "",
            year: "",
            duration: "",
            genre: "",
            rating: "",
            review: "",
            image_url: "",
        })

        setIsUpdate(false)
    }

    const onCreate = () => {
        reset()
        handleDialogOpen();
    }

    const handleDialogOpen = () => {
        setIsOpen(true);
    }

    const handleDialogClose = () => {
        setIsOpen(false);
    };
    
    if (session.username === null) {
        return (
            <div>
                <h1>Anda Belum Login</h1>
                <h1>Login untuk melanjutkan ke fitur ini</h1>
            </div>
        )
    }

    return (
        <div>
            <Box display="flex" flexDirection="row-reverse" mb={2} mt={2}>
                <Button variant="contained" color="primary" onClick={onCreate}>
                    Add Movie
                </Button>
            </Box>

            <div style={{ maxWidth: "100%" }}>
                <TableMovie
                    data={
                        listMovie
                            .map(item => {
                                const newItem = item;
                                if(newItem.description !== null){
                                    newItem.description = item.description.substr(0, 100);
                                }

                                return newItem;
                            })
                            .sort((a, b) => a.rating < b.rating ? 1 : -1)
                    }
                    actions={[
                        {
                            icon: () => <IconEdit color="primary" />,
                            tooltip: "Update",
                            onClick: (event, rowData) => onEdit(rowData.id, event)
                        },
                        {
                            icon: () => <IconDeleteOutline color="secondary" />,
                            tooltip: "Delete",
                            onClick: (event, rowData) => onDelete(rowData.id, event)
                        }
                    ]}
                />
            </div>

            <FormDialog
                isOpen={isOpen}
                handleClose={handleDialogClose} />
        </div>
    )

}

export default MovieList