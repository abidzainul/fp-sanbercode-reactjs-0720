import React, { useState, createContext } from 'react';

export const MovieContext = createContext();

export const MovieProvider = props => {
    const [movies, setMovies] = useState([]);
    const [movie, setMovie] = useState({
        id: "",
        title: "",
        description: "",
        year: "",
        duration: "",
        genre: "",
        rating: "",
        review: "",
        image_url: "",
    })
    const [isUpdate, setIsUpdate] = useState(false)

    return (
        <MovieContext.Provider
            value={{
                list: [movies, setMovies],
                data: [movie, setMovie],
                update: [isUpdate, setIsUpdate]
            }}>

            {props.children}
        </MovieContext.Provider>
    );
};