import React, { Component, useEffect, useContext } from 'react';

import { MovieProvider } from './MovieProvider';
import MovieList from './MovieList'
import FormDialogMovie from './FormDialogMovie'

class Movies extends Component {
    
    componentDidMount(){
        document.title = "Managemen Movies"
    }

    render(){
        return (
            <MovieProvider>
                <MovieList />
                <FormDialogMovie />
            </MovieProvider>
        )
    }
}

export default Movies