import http from './ApiConfig';

// =============== MOVIES ================
export const getAllMovies = () => http.get(`/movies`)
export const getMovie = id => http.get(`/movies/${id}`)
export const insertMovie = data => http.post(`/movies`, data)
export const updateMovie = (id, data) => http.put(`/movies/${id}`, data)
export const removeMovie = id => http.delete(`/movies/${id}`)

// =============== GAMES ================
export const getAllGames = () => http.get(`/games`)
export const getGame = id => http.get(`/games/${id}`)
export const insertGame = data => http.post(`/games`, data)
export const updateGame = (id, data) => http.put(`/games/${id}`, data)
export const removeGame = id => http.delete(`/games/${id}`)

// =============== USER ================
export const getAllUsers = () => http.get(`/users`)
export const getUser = id => http.get(`/users/${id}`)
export const insertUser = data => http.post(`/users`, data)
export const updateUser = (id, data) => http.put(`/users/${id}`, data)
export const removeUser = id => http.delete(`/users/${id}`)
export const signin = data => http.post(`/login`, data)


